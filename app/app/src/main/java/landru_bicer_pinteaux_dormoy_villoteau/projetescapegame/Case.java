package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.widget.ImageView;


public class Case {
    private int couleur; // 0:commun 1:bleu 2:rouge 3:vert
    private boolean etat; //0:ferme 1:ouvert
    private ImageView image, ipont1, ipont2;
    private Case case_haut, case_bas, case_gauche, case_droite, pont1, pont2;

    public Case(int couleur, boolean etat, ImageView image) {
        this.couleur = couleur;
        this.etat = etat;
        this.image = image;
    }

    public void setEtat(boolean etat) {
        this.etat = etat;
    }

    public void setCase_haut(Case case_dessus) {
        this.case_haut = case_dessus;
    }

    public void setCase_bas(Case case_dessous) {
        this.case_bas = case_dessous;
    }

    public void setCase_gauche(Case case_gauche) {
        this.case_gauche = case_gauche;
    }

    public void setCase_droite(Case case_droite) {
        this.case_droite = case_droite;
    }

    public int getCouleur() {
        return couleur;
    }

    public boolean isEtat() {
        return etat;
    }

    public ImageView getImage() {
        return image;
    }

    public Case getCase_haut() {
        return case_haut;
    }

    public Case getCase_bas() {
        return case_bas;
    }

    public Case getCase_gauche() {
        return case_gauche;
    }

    public Case getCase_droite() {
        return case_droite;
    }

    public ImageView getIpont1() {
        return ipont1;
    }

    public ImageView getIpont2() {
        return ipont2;
    }

    public Case getPont1() {
        return pont1;
    }

    public Case getPont2() {
        return pont2;
    }

    public void setPont(Case pont1, Case pont2, ImageView ipont1, ImageView ipont2) {
        this.pont1 = pont1;
        this.pont2 = pont2;
        this.ipont1 = ipont1;
        this.ipont2 = ipont2;
    }


}
