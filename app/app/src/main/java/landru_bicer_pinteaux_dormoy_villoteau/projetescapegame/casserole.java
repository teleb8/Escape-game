package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class casserole extends AppCompatActivity {

    private Button retour, manger;
    private TextView console;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_casserole);
        retour = findViewById(R.id.retour);
        manger = findViewById(R.id.manger);
        console = findViewById(R.id.text);
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
        manger.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaPlayer bruit = MediaPlayer.create(getApplicationContext(), R.raw.cri);
                bruit.start();
                console.setText("La nourriture est froide et immonde");

            }
        });
    }
}
