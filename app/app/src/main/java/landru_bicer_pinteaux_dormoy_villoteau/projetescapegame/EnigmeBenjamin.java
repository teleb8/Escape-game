package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.chrisbanes.photoview.PhotoView;

import static java.lang.Thread.sleep;

//Author Alexis
public class EnigmeBenjamin extends AppCompatActivity {
    private ImageView Zoom2;
    private Button valider, retour, recup;
    private EditText reponse;
    private int s, succes;
    private MediaPlayer bruit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enigme_benjamin);

        valider = (Button) findViewById(R.id.valider);
        recup = (Button) findViewById(R.id.recup);
        recup.setVisibility(View.INVISIBLE);
        retour = (Button) findViewById(R.id.retour);
        reponse = (EditText) findViewById(R.id.reponse);
        s = 0;
        final PhotoView photoView = (PhotoView) findViewById(R.id.photo_view);
        photoView.setImageResource(R.drawable.planbenjam);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    s = (int) Integer.parseInt(reponse.getText().toString());
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }


                if (s == 29) {
                    retour.setVisibility(View.INVISIBLE);
                    reponse.setVisibility(View.INVISIBLE);
                    valider.setVisibility(View.INVISIBLE);
                    photoView.setImageResource(R.drawable.cle_noire);
                    recup.setVisibility(View.VISIBLE);
                } else {
                }
            }
        });
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
        recup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.secret);
                bruit.start();
                succes = 3;
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", succes);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}
