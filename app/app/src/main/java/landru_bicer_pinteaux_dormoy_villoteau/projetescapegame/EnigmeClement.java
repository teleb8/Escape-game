package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import static java.lang.Thread.sleep;

public class EnigmeClement extends AppCompatActivity{
    private MediaPlayer bruit;
    private int succes;
    //layouts
    private ConstraintLayout lay_enigme1, lay_enigme2, lay_enigme3, lay_runes, lay_conte;

    //booleans pour les trois objets à récupérer + leur affichage
    private boolean baguette, pierre, cape;

    private String value, heure, minute;

    //elements du layout principal
    private Button btn_tombe,btn_arbre,btn_eau,btn_pierre,btn_horloge,btn_autel,retour,btn_conte,recup;
    private TextView tv_rien,tv_e1;
    private ImageView img_cadre,img_baguette,img_cape,img_pierre;

    //elements pour le layout de la seconde enigme
    private Button btn_val_pierre, btn_retour_e2, btn_table;
    private EditText ed_rep_pierre;
    private TextView tv_enigme2_rate;
    private ImageView img_pierre_rune;

    //elements pour le layout de la troisieme enigme
    private Button btn_hp, btn_hm, btn_mp, btn_mn, btn_valider3, btn_retour3;
    private TextView tv_heure, tv_min, tv_e3_rate;

    //elements pour le layout runes
    private Button btn_ok;

    //elements pour le layout conte
    private Button btn_ok_conte;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enigme_clement);

        //récupération des layouts
        lay_enigme1 = findViewById(R.id.layout_enigmeClement);
        lay_enigme2 = findViewById(R.id.layout_enigme2);
        lay_enigme3 = findViewById(R.id.layout_enigme3);
        lay_runes = findViewById(R.id.layout_runes);
        lay_conte = findViewById(R.id.layout_conte);

        //assignation des valeurs des booleans, à false, puisqu'on ne les a pas encore récupérés
        baguette = false;
        pierre = false;
        cape = false;

        value = "";

        //récupération des éléments du layout principal
        recup=(Button)findViewById(R.id.recup);
        recup.setVisibility(View.INVISIBLE);
        btn_tombe =  findViewById(R.id.bouton_tombe);
        btn_arbre = findViewById(R.id.bouton_arbre);
        btn_eau = findViewById(R.id.bouton_eau);
        btn_pierre = findViewById(R.id.bouton_pierre);
        btn_horloge = findViewById(R.id.bouton_horloge);
        btn_autel = findViewById(R.id.bouton_autel);
        img_cadre = findViewById(R.id.cadre);
        img_baguette = findViewById(R.id.trait);
        img_baguette.setVisibility(View.INVISIBLE);
        img_pierre = findViewById(R.id.cercle);
        img_pierre.setVisibility(View.INVISIBLE);
        img_cape = findViewById(R.id.triangle);
        img_cape.setVisibility(View.INVISIBLE);
        btn_conte = findViewById(R.id.conte);

        //récupération des éléments du layout pour l'énigme 2
        btn_val_pierre = findViewById(R.id.btn_validation_pierre);
        btn_table = findViewById(R.id.btn_tableTrad);
        btn_retour_e2 = findViewById(R.id.btn_retour_enigme2);
        ed_rep_pierre = findViewById(R.id.text_rep_pierre);
        tv_enigme2_rate = findViewById(R.id.text_enigme2_rate);
        tv_enigme2_rate.setVisibility(View.INVISIBLE);
        img_pierre_rune = findViewById(R.id.pierre_runes);
        disableLayout2();

        tv_rien = findViewById(R.id.text_rien);
        tv_rien.setVisibility(View.INVISIBLE);
        tv_e1 = findViewById(R.id.text_enigme1);
        tv_e1.setVisibility(View.INVISIBLE);

        //récupération des éléments du layout pour l'enigme 3
        btn_hp = findViewById(R.id.bouton_heure_plus);
        btn_hm = findViewById(R.id.bouton_heure_moins);
        btn_mp = findViewById(R.id.bouton_min_plus);
        btn_mn = findViewById(R.id.bouton_min_moins);
        btn_valider3 = findViewById(R.id.bouton_valider_heure);
        btn_retour3 = findViewById(R.id.bouton_retour3);
        tv_heure = findViewById(R.id.heure);
        tv_min = findViewById(R.id.minute);
        tv_e3_rate = findViewById(R.id.enigme3_rate);
        tv_e3_rate.setVisibility(View.INVISIBLE);
        disableLayout3();

        //récupération des éléments du layout pour la table de traduction
        btn_ok = findViewById(R.id.btn_ok);
        disableLayoutR();

        //récupération des éléments du layout pour le conte des trois frères
        btn_ok_conte = findViewById(R.id.btn_conte_ok);
        disableLayoutC();

//action inspecter la tombe->rien ne se passe si l'énigme n'est pas résolue
        btn_tombe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (baguette && pierre && cape) {
                    btn_tombe.setClickable(false);
                    btn_tombe.setBackgroundColor(Color.GRAY);
                    btn_arbre.setVisibility(View.INVISIBLE);
                    btn_autel.setVisibility(View.INVISIBLE);
                    btn_conte.setVisibility(View.INVISIBLE);
                    btn_eau.setVisibility(View.INVISIBLE);
                    btn_horloge.setVisibility(View.INVISIBLE);
                    btn_tombe.setVisibility(View.INVISIBLE);
                    btn_pierre.setVisibility(View.INVISIBLE);
                    retour.setVisibility(View.INVISIBLE);
                    recup.setVisibility(View.VISIBLE);
                    img_cadre.setImageResource(R.drawable.cle_bleu);
                }

            }
        });
        recup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bruit= MediaPlayer.create(getApplicationContext(),R.raw.secret);
                bruit.start();
                succes=2;
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result",succes);
                setResult(RESULT_OK,returnIntent);
                finish();
            }
        });


//action click sur l'arbre-> donne une objet conclure l'énigme1 et n'est plus clickable une fois appelé
        btn_arbre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //tv_e1.setVisibility(View.VISIBLE);
                baguette();
                btn_arbre.setClickable(false);
                btn_arbre.setBackgroundColor(Color.GRAY);
            }
        });

//action click dans l'eau-> interaction inutile, le bouton devient non clickable
        btn_eau.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_rien.setVisibility(View.INVISIBLE);
                btn_eau.setClickable(false);
                btn_eau.setBackgroundColor(Color.GRAY);
            }
        });

//action changer l'heure de l'horloge->si heure bonne : fin de l'énigme2, sinon rien ne se passe + message
        btn_horloge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableLayout1();
                enableLayout3();
            }
        });

//action lire la pierre
        btn_pierre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableLayout1();
                enableLayout2();
            }
        });

//action écrire le mot "magie" énigme de la pierre
        btn_val_pierre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                value=ed_rep_pierre.getText().toString();
                value=value.toUpperCase();
                if(value.equals("MAGIE")){
                    pierre();
                    tv_enigme2_rate.setVisibility(View.INVISIBLE);
                    disableLayout2();
                    enableLayout1();
                    btn_pierre.setClickable(false);
                    btn_pierre.setBackgroundColor(Color.GRAY);
                } else {
                    tv_enigme2_rate.setVisibility(View.VISIBLE);
                }
            }
        });

        btn_retour_e2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableLayout2();
                enableLayout1();
            }
        });

        btn_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableLayoutR();
                disableLayout2();
            }
        });

//action inspecter l'autel
        btn_autel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btn_autel.setBackgroundColor(Color.GRAY);
                btn_autel.setClickable(false);
            }
        });

        retour = findViewById(R.id.retour);
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), Tableau2.class);
                startActivity(myIntent);
                finish();
            }
        });
        //partie enigme3
        btn_hp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int h;
                heure = tv_heure.getText().toString();
                h = Integer.parseInt(heure);
                h = h + 1;
                if (h == 24) {
                    h = 0;
                }
                if (h < 10) {
                    tv_heure.setText("0" + String.valueOf(h));
                } else {
                    tv_heure.setText(String.valueOf(h));
                }
            }
        });

        btn_hm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int h;
                heure = tv_heure.getText().toString();
                h = Integer.parseInt(heure);
                h = h - 1;
                if (h < 0) {
                    h = 23;
                }
                if (h < 10) {
                    tv_heure.setText("0" + String.valueOf(h));
                } else {
                    tv_heure.setText(String.valueOf(h));
                }
            }
        });

        btn_mp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int m;
                minute = tv_min.getText().toString();
                m = Integer.parseInt(minute);
                m = m + 1;
                if (m == 60) {
                    m = 0;
                }
                if (m < 10) {
                    tv_min.setText("0" + String.valueOf(m));
                } else {
                    tv_min.setText(String.valueOf(m));
                }
            }
        });

        btn_mn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int m;
                minute = tv_min.getText().toString();
                m = Integer.parseInt(minute);
                m = m - 1;
                if (m == -1) {
                    m = 59;
                }
                if (m < 10) {
                    tv_min.setText("0" + String.valueOf(m));
                } else {
                    tv_min.setText(String.valueOf(m));
                }
            }
        });

        btn_valider3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                heure = tv_heure.getText().toString();
                minute = tv_min.getText().toString();
                if ((heure.equals("23")) && (minute.equals("59"))) {
                    cape();
                    disableLayout3();
                    enableLayout1();
                    btn_horloge.setClickable(false);
                    btn_horloge.setBackgroundColor(Color.GRAY);
                } else {
                    tv_e3_rate.setVisibility(View.VISIBLE);
                }
            }
        });

        btn_retour3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableLayout3();
                enableLayout1();
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableLayoutR();
                enableLayout2();
            }
        });

        btn_conte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enableLayoutC();
            }
        });

        btn_ok_conte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                disableLayoutC();
            }
        });

    }

    public void enableLayout1() {
        lay_enigme1.setVisibility(View.VISIBLE);
        btn_tombe.setVisibility(View.VISIBLE);
        btn_arbre.setVisibility(View.VISIBLE);
        btn_eau.setVisibility(View.VISIBLE);
        btn_pierre.setVisibility(View.VISIBLE);
        btn_horloge.setVisibility(View.VISIBLE);
        btn_autel.setVisibility(View.VISIBLE);
        img_cadre.setVisibility(View.VISIBLE);
        retour.setVisibility(View.VISIBLE);
    }

    public void disableLayout1() {
        lay_enigme1.setVisibility(View.INVISIBLE);
        btn_tombe.setVisibility(View.INVISIBLE);
        btn_arbre.setVisibility(View.INVISIBLE);
        btn_eau.setVisibility(View.INVISIBLE);
        btn_pierre.setVisibility(View.INVISIBLE);
        btn_horloge.setVisibility(View.INVISIBLE);
        btn_autel.setVisibility(View.INVISIBLE);
        img_cadre.setVisibility(View.INVISIBLE);
        retour.setVisibility(View.INVISIBLE);
    }

    public void enableLayout2() {
        lay_enigme2.setVisibility(ConstraintLayout.VISIBLE);
        btn_val_pierre.setVisibility(View.VISIBLE);
        btn_table.setVisibility(View.VISIBLE);
        btn_retour_e2.setVisibility(View.VISIBLE);
        img_pierre_rune.setVisibility(View.VISIBLE);
    }

    public void disableLayout2() {
        lay_enigme2.setVisibility(ConstraintLayout.INVISIBLE);
        btn_val_pierre.setVisibility(View.INVISIBLE);
        btn_table.setVisibility(View.INVISIBLE);
        btn_retour_e2.setVisibility(View.INVISIBLE);
        img_pierre_rune.setVisibility(View.INVISIBLE);
    }

    public void enableLayout3() {
        lay_enigme3.setVisibility(View.VISIBLE);
        btn_hp.setVisibility(View.VISIBLE);
        btn_hm.setVisibility(View.VISIBLE);
        btn_mp.setVisibility(View.VISIBLE);
        btn_mn.setVisibility(View.VISIBLE);
        btn_valider3.setVisibility(View.VISIBLE);
        btn_retour3.setVisibility(View.VISIBLE);
        tv_heure.setVisibility(View.VISIBLE);
        tv_min.setVisibility(View.VISIBLE);
    }

    public void disableLayout3() {
        lay_enigme3.setVisibility(View.INVISIBLE);
        btn_hp.setVisibility(View.INVISIBLE);
        btn_hm.setVisibility(View.INVISIBLE);
        btn_mp.setVisibility(View.INVISIBLE);
        btn_mn.setVisibility(View.INVISIBLE);
        btn_valider3.setVisibility(View.INVISIBLE);
        btn_retour3.setVisibility(View.INVISIBLE);
        tv_heure.setVisibility(View.INVISIBLE);
        tv_min.setVisibility(View.INVISIBLE);
    }

    public void enableLayoutR() {
        lay_runes.setVisibility(View.VISIBLE);
        btn_ok.setVisibility(View.VISIBLE);
    }

    public void disableLayoutR() {
        lay_runes.setVisibility(View.INVISIBLE);
        btn_ok.setVisibility(View.INVISIBLE);
    }

    public void enableLayoutC() {
        lay_conte.setVisibility(View.VISIBLE);
        btn_ok_conte.setVisibility(View.VISIBLE);
    }

    public void disableLayoutC() {
        lay_conte.setVisibility(View.INVISIBLE);
        btn_ok_conte.setVisibility(View.INVISIBLE);
    }

    public void baguette() {
        baguette = true;
        img_baguette.setVisibility(View.VISIBLE);
    }

    public void cape() {
        cape = true;
        img_cape.setVisibility(View.VISIBLE);
    }

    public void pierre() {
        pierre = true;
        img_pierre.setVisibility(View.VISIBLE);
    }
}
