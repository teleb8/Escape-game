package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.VideoView;

import static java.lang.Thread.sleep;

public class Final extends AppCompatActivity {
    private VideoView videoView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final);
        videoView = (VideoView) findViewById(R.id.videoView3);
        playVideo();
        Thread attente = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            sleep(6000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
                        startActivity(myIntent);
                        finish();
                    }
                }
        );
        attente.start();
    }

    public void playVideo() {
        //MediaController m = new MediaController(this);
        //videoView.setMediaController(m);
        String path = "android.resource://" + this.getPackageName() + "/" + R.raw.fin;
        Uri u = Uri.parse(path);
        videoView.setVideoURI(u);
        videoView.requestFocus();
        videoView.start();
    }
}
