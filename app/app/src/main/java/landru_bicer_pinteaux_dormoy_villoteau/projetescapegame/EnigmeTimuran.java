package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import static java.lang.Thread.sleep;

public class EnigmeTimuran extends AppCompatActivity {
    private Button retour, valider, recup;
    private EditText rep;
    private String s;
    private MediaPlayer bruit;
    private int succes;
    private ImageView img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enigme_timuran);
        valider=(Button)findViewById(R.id.valider);
        img=(ImageView) findViewById(R.id.imageView3);
        recup=(Button)findViewById(R.id.recup);
        recup.setVisibility(View.INVISIBLE);
        rep = (EditText) findViewById(R.id.rep);
        retour = (Button) findViewById(R.id.retour);
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    s = (String) rep.getText().toString();
                    s = s.toUpperCase();
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                if (s.equals("HVO")) {
                    recup.setVisibility(View.VISIBLE);
                    retour.setVisibility(View.INVISIBLE);
                    valider.setVisibility(View.INVISIBLE);
                    img.setImageResource(R.drawable.cle_jaune);
                }
            }
        });
        recup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.secret);
                bruit.start();
                succes = 4;
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", succes);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }
}
