package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import icepick.Icepick;
import icepick.State;

import static java.lang.Thread.sleep;

//Author Alexis
public class EnigmeAlexis extends AppCompatActivity {
    //Déclaration variable
    private Button livre_Bleu, livre_Rouge, retour;
    private Button recup, valider;
    private EditText reponse;
    private String s;

    private TextView indice;
    private int succes;
    private ImageView image;
    private MediaPlayer bruit;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enigme_alexis);
        //Récuperation des données vie leur Id
        livre_Bleu = (Button) findViewById(R.id.livre_Bleu);
        livre_Rouge = (Button) findViewById(R.id.livre_Rouge);
        valider = (Button) findViewById(R.id.valider);
        retour = (Button) findViewById(R.id.retour);
        recup = (Button) findViewById(R.id.recuperer_cle);
        recup.setVisibility(View.INVISIBLE);
        reponse = (EditText) findViewById(R.id.reponse);
        s = "";
        image = (ImageView) findViewById(R.id.Img);

        //Affichage du livre bleu
        livre_Bleu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.page);
                bruit.start();
                Intent myIntent = new Intent(getApplicationContext(), Livre_bleu.class);
                startActivity(myIntent);
            }
        });
        //Affichage du livre rouge
        livre_Rouge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.page);
                bruit.start();
                Intent myIntent = new Intent(getApplicationContext(), Livre_rouge.class);
                startActivity(myIntent);
            }
        });
        //Envoie d'une reponse et validation ou non de celle-ci
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    s = (String) reponse.getText().toString();
                    s = s.toUpperCase();
                } catch (NumberFormatException nfe) {
                    System.out.println("Could not parse " + nfe);
                }
                if (s.equals("NIHMULI")) {

                    reponse.setVisibility(View.INVISIBLE);
                    valider.setVisibility(View.INVISIBLE);
                    recup.setVisibility(View.VISIBLE);
                    retour.setVisibility(View.INVISIBLE);
                    livre_Bleu.setVisibility(View.INVISIBLE);
                    livre_Rouge.setVisibility(View.INVISIBLE);
                    image.setImageResource(R.drawable.test_cle);
                    bruit = MediaPlayer.create(getApplicationContext(), R.raw.deverouillage);
                    bruit.start();

                } else {
                    bruit = MediaPlayer.create(getApplicationContext(), R.raw.erreur);
                    bruit.start();
                    reponse.setText("");
                }

            }
        });
        //revenir en arriére
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
        //Ouvrir le coffre si déverouillé
        recup.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.secret);
                bruit.start();
                image.setImageResource(R.drawable.test_fin);
                succes = 1;
                recup.setVisibility(View.INVISIBLE);
                try {
                    sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                Intent returnIntent = new Intent();
                returnIntent.putExtra("result", succes);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
    }

    public int getSucces() {
        return succes;
    }

    public String getS() {
        return s;
    }

}
