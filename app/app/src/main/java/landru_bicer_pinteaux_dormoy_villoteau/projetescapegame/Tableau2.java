package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.MediaController;

//author Alexis
public class Tableau2 extends AppCompatActivity {
    private ImageView img;
    //Tab2
    private Button coffre, gauche, porte, cadre, tonneau_1, tonneau_2;
    //Tab3
    private Button droite, cadre2, panier, parchemin, casserole;
    private int Compteur;
    private MediaPlayer bruit;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tableau2);
        Compteur = 0;
        img = (ImageView) findViewById(R.id.Image);
        //Tableau2
        gauche = (Button) findViewById(R.id.gauche);
        tonneau_1 = (Button) findViewById(R.id.tonneau_1);
        tonneau_2 = (Button) findViewById(R.id.tonneau_2);
        cadre = (Button) findViewById(R.id.cadret1);
        coffre = (Button) findViewById(R.id.coffre);
        coffre.setEnabled(false);
        porte = (Button) findViewById(R.id.porte);
        porte.setEnabled(false);

        //Tableau3
        droite = (Button) findViewById(R.id.droite);
        droite.setVisibility(View.INVISIBLE);
        cadre2 = (Button) findViewById(R.id.cadret2);
        cadre2.setVisibility(View.INVISIBLE);
        panier = (Button) findViewById(R.id.panier);
        panier.setVisibility(View.INVISIBLE);
        parchemin = (Button) findViewById(R.id.parchemin);
        parchemin.setVisibility(View.INVISIBLE);
        casserole = (Button) findViewById(R.id.casserole);
        casserole.setVisibility(View.INVISIBLE);

        //Changement vers Tableau3
        gauche.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.pas);
                bruit.start();
                img.setImageResource(R.drawable.tableau3);
                gauche.setVisibility(View.INVISIBLE);
                tonneau_1.setVisibility(View.INVISIBLE);
                tonneau_2.setVisibility(View.INVISIBLE);
                cadre.setVisibility(View.INVISIBLE);
                cadre.setVisibility(View.INVISIBLE);
                coffre.setVisibility(View.INVISIBLE);
                porte.setVisibility(View.INVISIBLE);
                parchemin.setVisibility(View.VISIBLE);
                panier.setVisibility(View.VISIBLE);
                cadre2.setVisibility(View.VISIBLE);
                droite.setVisibility(View.VISIBLE);
                ;
                casserole.setVisibility(View.VISIBLE);
            }
        });
        //Changement vers Tableau2
        droite.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.pas);
                bruit.start();
                img.setImageResource(R.drawable.tableau1);
                gauche.setVisibility(View.VISIBLE);
                tonneau_1.setVisibility(View.VISIBLE);
                tonneau_2.setVisibility(View.VISIBLE);
                cadre.setVisibility(View.VISIBLE);
                cadre.setVisibility(View.VISIBLE);
                coffre.setVisibility(View.VISIBLE);
                porte.setVisibility(View.VISIBLE);
                parchemin.setVisibility(View.INVISIBLE);
                panier.setVisibility(View.INVISIBLE);
                cadre2.setVisibility(View.INVISIBLE);
                droite.setVisibility(View.INVISIBLE);
                casserole.setVisibility(View.INVISIBLE);
            }
        });
        //Déclenchement des enigmes tab2
        //Declenchement de l'enigme d'alexis
        coffre.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.coffre);
                bruit.start();
                Intent myIntent = new Intent(getApplicationContext(), EnigmeAlexis.class);
                startActivityForResult(myIntent, 1);
            }
        });
        //Declenchement de l'enigme de clément
        tonneau_1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.osier);
                bruit.start();
                coffre.setEnabled(true);
                tonneau_1.setEnabled(false);
            }
        });
        tonneau_2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                bruit = MediaPlayer.create(getApplicationContext(), R.raw.osier);
                bruit.start();
                Intent myIntent = new Intent(getApplicationContext(), Panier.class);
                startActivity(myIntent);
            }
        });
        cadre.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), EnigmeClement.class);
                startActivityForResult(myIntent, 2);
            }
        });
        //Déclenchement des enigmes tab3
        //Declenchement de l'enigme de benjamin
        cadre2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), EnigmeBenjamin.class);
                startActivityForResult(myIntent, 3);
            }
        });
        //Declenchement de l'enigme de Pierre
        panier.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), EnigmePierre.class);
                startActivityForResult(myIntent, 5);
            }
        });
        //Declenchement de l'enigme de timuran
        parchemin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), EnigmeTimuran.class);
                startActivityForResult(myIntent, 4);

            }
        });
        //Direction l'enigme final
        porte.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(),Transition.class);
                startActivity(myIntent);
                finish();
            }
        });
        casserole.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent myIntent = new Intent(getApplicationContext(), casserole.class);
                startActivity(myIntent);
            }
        });
    }

    //Verouillage enigme
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK && requestCode == 1) {
            coffre.setEnabled(false);
            Porte();
        }
        if (resultCode == RESULT_OK && requestCode == 2) {
            cadre.setEnabled(false);
            Porte();
        }
        if (resultCode == RESULT_OK && requestCode == 3) {
            cadre2.setEnabled(false);
            Porte();
        }
        if (resultCode == RESULT_OK && requestCode == 4) {
            parchemin.setEnabled(false);
            Porte();
        }
        if (resultCode == RESULT_OK && requestCode == 5) {
            panier.setEnabled(false);
            Porte();
        }
    }
    public void Porte(){
        Compteur=Compteur+1;
        if(Compteur==5){
            porte.setEnabled(true);
        }
    }
}
