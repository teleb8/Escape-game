package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Panier extends AppCompatActivity {
    private Button retour, boire;
    private TextView console;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panier);
        retour = findViewById(R.id.retour);
        boire = findViewById(R.id.boire);
        console = findViewById(R.id.text);
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
        boire.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                MediaPlayer bruit = MediaPlayer.create(getApplicationContext(), R.raw.hocquet);
                bruit.start();
                console.setText("Le vin est delicieux");

            }
        });
    }
}
