package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

//Author Alexis
public class Livre_bleu extends AppCompatActivity {
    private Button quitter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_livre_bleu);
        quitter = (Button) findViewById(R.id.quitter);
        quitter.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                finish();
            }
        });
    }
}
