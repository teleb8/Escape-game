package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import static java.lang.Thread.sleep;

public class Intro extends AppCompatActivity {
    private VideoView videoView;
    private Button skip;
    private int passer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);

        videoView = (VideoView) findViewById(R.id.videoView);
        skip = (Button) findViewById(R.id.skip);
        passer = 0;

        playVideo();
        Thread attente = new Thread(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            sleep(19000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        if (passer == 0) {
                            Intent myIntent = new Intent(getApplicationContext(), Tableau2.class);
                            startActivity(myIntent);
                            finish();
                        }

                    }
                }
        );
        attente.start();

        skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passer = 1;
                Intent myIntent = new Intent(getApplicationContext(), Tableau2.class);
                startActivity(myIntent);
                finish();
            }
        });
    }

    public void playVideo() {
        //MediaController m = new MediaController(this);
        //videoView.setMediaController(m);

        String path = "android.resource://" + this.getPackageName() + "/" + R.raw.intro;

        Uri u = Uri.parse(path);

        videoView.setVideoURI(u);

        videoView.requestFocus();
        videoView.start();

    }
}
