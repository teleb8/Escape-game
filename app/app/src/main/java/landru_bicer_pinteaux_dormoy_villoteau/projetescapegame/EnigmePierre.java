package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import static java.lang.Thread.sleep;

public class EnigmePierre extends AppCompatActivity {

    private Button haut, gauche, bas, droite, action, retour;
    private boolean rouge, bleu, vert;
    private Case c21, c52, c22, c12, c23, c20, c24, c02, c26, c44, c66, c46, c40, c45, c30, c42, c62, c64, c41, c34, fin, c36, c56, c65, c63, c72;
    private Pion joueur;
    private EnigmePierre activity;
    private int succes;
    private MediaPlayer bruit;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enigme_pierre);
        this.activity = this;

        c21 = new Case(0, true, (ImageView) findViewById(R.id.j21));
        c23 = new Case(0, true, (ImageView) findViewById(R.id.j23));
        c22 = new Case(0, true, (ImageView) findViewById(R.id.j22));
        c12 = new Case(0, true, (ImageView) findViewById(R.id.j12));
        c02 = new Case(0, true, ((ImageView) findViewById(R.id.j02)));
        c20 = new Case(0, true, ((ImageView) findViewById(R.id.j20)));
        c24 = new Case(1, true, ((ImageView) findViewById(R.id.j24)));
        c26 = new Case(2, true, ((ImageView) findViewById(R.id.j26)));
        c36 = new Case(0, false, ((ImageView) findViewById(R.id.j36)));
        c34 = new Case(0, true, ((ImageView) findViewById(R.id.j34)));
        c30 = new Case(0, false, ((ImageView) findViewById(R.id.j30)));
        c40 = new Case(3, true, ((ImageView) findViewById(R.id.j40)));
        c41 = new Case(0, true, ((ImageView) findViewById(R.id.j41)));
        c42 = new Case(0, true, ((ImageView) findViewById(R.id.j42)));
        c44 = new Case(0, true, ((ImageView) findViewById(R.id.j44)));
        c45 = new Case(0, true, ((ImageView) findViewById(R.id.j45)));
        c46 = new Case(0, true, ((ImageView) findViewById(R.id.j46)));
        c56 = new Case(0, true, ((ImageView) findViewById(R.id.j56)));
        c52 = new Case(0, true, ((ImageView) findViewById(R.id.j52)));
        c62 = new Case(0, true, ((ImageView) findViewById(R.id.j62)));
        c63 = new Case(0, true, ((ImageView) findViewById(R.id.j63)));
        c64 = new Case(0, true, ((ImageView) findViewById(R.id.j64)));
        c65 = new Case(0, true, ((ImageView) findViewById(R.id.j65)));
        c66 = new Case(0, true, ((ImageView) findViewById(R.id.j66)));
        c72 = new Case(0, false, (ImageView) findViewById(R.id.j72));
        fin = new Case(4, true, (ImageView) findViewById(R.id.fin));

        fin.setCase_bas(c72);

        c72.setCase_bas(c62);
        c72.setCase_haut(fin);

        c66.setCase_gauche(c65);
        c66.setCase_bas(c56);

        c65.setCase_gauche(c64);
        c65.setCase_droite(c66);

        c64.setCase_gauche(c63);
        c64.setCase_droite(c65);

        c63.setCase_droite(c64);
        c63.setCase_gauche(c62);

        c62.setCase_bas(c52);
        c62.setCase_haut(c72);
        c62.setCase_droite(c63);

        c52.setCase_bas(c42);
        c52.setCase_haut(c62);

        c56.setCase_haut(c66);
        c56.setCase_bas(c46);

        c46.setCase_gauche(c45);
        c46.setCase_bas(c36);
        c46.setCase_haut(c56);

        c45.setCase_droite(c46);
        c45.setCase_gauche(c44);

        c44.setCase_bas(c34);
        c44.setCase_droite(c45);

        c42.setCase_gauche(c41);
        c42.setCase_haut(c52);

        c41.setCase_droite(c42);
        c41.setCase_gauche(c40);

        c40.setCase_bas(c30);
        c40.setCase_droite(c41);

        c30.setCase_haut(c40);
        c30.setCase_bas(c20);

        c34.setCase_bas(c24);
        c34.setCase_haut(c44);

        c36.setCase_haut(c46);
        c36.setCase_bas(c26);

        c26.setCase_haut(c36);

        c24.setCase_haut(c34);
        c24.setCase_gauche(c23);

        c20.setCase_droite(c21);
        c20.setCase_haut(c30);

        c21.setCase_droite(c22);
        c21.setCase_gauche(c20);

        c23.setCase_droite(c24);
        c23.setCase_gauche(c22);

        c22.setCase_droite(c23);
        c22.setCase_gauche(c21);
        c22.setCase_bas(c12);

        c12.setCase_haut(c22);
        c12.setCase_bas(c02);

        c02.setCase_haut(c12);

        c24.setPont(c34, c30, (ImageView) findViewById(R.id.c34), (ImageView) findViewById(R.id.c30));

        c40.setPont(c52, c36, (ImageView) findViewById(R.id.c52), (ImageView) findViewById(R.id.c36));

        c26.setPont(c72, c63, (ImageView) findViewById(R.id.c72), (ImageView) findViewById(R.id.c63));


        joueur = new Pion(c02);


        haut = findViewById(R.id.haut);
        haut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View a) {

                if (joueur.getPosition() == c72) {
                            bruit= MediaPlayer.create(getApplicationContext(),R.raw.secret);
                            bruit.start();
                            try {
                                sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            succes=5;
                            Intent returnIntent = new Intent();
                            returnIntent.putExtra("result", succes);
                            setResult(RESULT_OK, returnIntent);
                            finish();

                    }


                else {
                    joueur.avance(((joueur.getPosition()).getCase_haut()));

                }
            }
        });

        bas = findViewById(R.id.bas);
        bas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View b) {

                joueur.avance((joueur.getPosition()).getCase_bas());
            }
        });

        gauche = findViewById(R.id.gauche);
        gauche.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View c) {

                joueur.avance((joueur.getPosition()).getCase_gauche());
            }
        });


        droite = findViewById(R.id.droite);
        droite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View d) {

                joueur.avance((joueur.getPosition()).getCase_droite());


            }
        });
        action = findViewById(R.id.action);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View d) {

                joueur.act();


            }
        });


        retour = (Button) findViewById(R.id.retour);
        retour.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
    }
}
