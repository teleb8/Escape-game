package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;

import android.content.Intent;
import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import java.io.IOException;

//author Alexis
public class MainActivity extends AppCompatActivity {

    private Button jouer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        jouer = (Button) findViewById(R.id.jouer);
        playsound();
        jouer.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                //Intent myIntent = new Intent(getApplicationContext(), Nikzebi.class);
                Intent myIntent = new Intent(getApplicationContext(), Intro.class);
                startActivity(myIntent);
                finish();
            }
        });

        //MediaPlayer son= MediaPlayer.create(getApplicationContext(),R.raw.ostmain);
        //son.start();


    }

    public void playsound() {

        MediaPlayer son = MediaPlayer.create(getApplicationContext(), R.raw.ostmain);
        son.start();
        son.setLooping(true);
    }

    public void stopsound() {
        MediaPlayer son = MediaPlayer.create(getApplicationContext(), R.raw.ostmain);
        son.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        playsound();
    }

    @Override
    public void onStop() {
        super.onStop();
        stopsound();
    }
}
