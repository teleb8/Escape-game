package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;


import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import static java.lang.Thread.sleep;

public class EnigmeFinal extends AppCompatActivity {
    private Button rouge, bleu, jaune, vert;
    private MediaPlayer bruit;
    private int compt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enigme_final);
        rouge = (Button) findViewById(R.id.crouge);
        bleu = (Button) findViewById(R.id.cbleu);
        jaune = (Button) findViewById(R.id.cjaune);
        vert = (Button) findViewById(R.id.cverte);

        compt = 0;

        rouge.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (compt == 0) {
                    rouge.setEnabled(false);
                    compt++;
                    bruit = MediaPlayer.create(getApplicationContext(), R.raw.cle);
                    bruit.start();
                } else {
                    echec();
                }
            }
        });
        bleu.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (compt == 2) {
                    bleu.setEnabled(false);
                    compt++;
                    bruit = MediaPlayer.create(getApplicationContext(), R.raw.cle);
                    bruit.start();
                } else {
                    echec();
                }
            }
        });
        jaune.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (compt == 3) {
                    jaune.setEnabled(false);
                    bruit = MediaPlayer.create(getApplicationContext(), R.raw.cle);
                    bruit.start();
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    bruit = MediaPlayer.create(getApplicationContext(), R.raw.reussite);
                    bruit.start();
                    try {
                        sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Intent myIntent = new Intent(getApplicationContext(), Final.class);
                    startActivity(myIntent);
                    finish();
                } else {
                    echec();
                }
            }
        });
        vert.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                if (compt == 1) {
                    vert.setEnabled(false);
                    compt++;
                    bruit = MediaPlayer.create(getApplicationContext(), R.raw.cle);
                    bruit.start();
                } else {
                    echec();
                }
            }
        });
    }

    public void echec() {
        compt = 0;
        bruit = MediaPlayer.create(getApplicationContext(), R.raw.lock);
        bruit.start();
        rouge.setEnabled(true);
        bleu.setEnabled(true);
        jaune.setEnabled(true);
        vert.setEnabled(true);
    }
}
