package landru_bicer_pinteaux_dormoy_villoteau.projetescapegame;
//classe permettant de connaitre la position du pion du joueur dans le cadrillage

import android.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;

public class Pion {
    Case position;

    public Pion(Case position) {
        this.position = position;

    }

    public Case getPosition() {
        return position;
    }

    public void avance(Case newpos) {
        try {
            if ((newpos.isEtat())) {
                ImageView image = position.getImage();
                image.setImageResource(0);
                image = newpos.getImage();
                image.setImageResource(R.drawable.poudlard);
                position = newpos;
            }
        } catch (java.lang.NullPointerException e) {

        }
    }

    public void act() {
        if (position.getCouleur() > 0) {
            Case pont = position.getPont1();
            ImageView image = position.getIpont1();
            if ((image.getRotation()) == 90) {
                image.setRotation(0);
            } else {
                image.setRotation(90);
            }
            if (pont.isEtat()) {
                pont.setEtat(false);

            } else {
                pont.setEtat(true);

            }

            pont = position.getPont2();
            image = position.getIpont2();
            if ((image.getRotation()) == 90) {
                image.setRotation(0);
            } else {
                image.setRotation(90);
            }
            if (pont.isEtat()) {
                pont.setEtat(false);
            } else {
                pont.setEtat(true);
            }
        }
    }


}
